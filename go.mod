module werematrix

go 1.15

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/alecthomas/repr v0.0.0-20201120212035-bb82daffcca2
	github.com/cosmtrek/air v1.15.1 // indirect
	github.com/facebook/ent v0.5.4
	github.com/hashicorp/go-immutable-radix v1.3.0
	github.com/logrusorgru/aurora v2.0.3+incompatible
	github.com/matrix-org/gomatrix v0.0.0-20200827122206-7dd5e2a05bcd
	github.com/mattn/go-sqlite3 v1.14.6
	golang.org/x/sys v0.0.0-20210123231150-1d476976d117 // indirect
)
