package bot

import (
	"time"

	"github.com/matrix-org/gomatrix"

	"werematrix/bot/commands"
	. "werematrix/bot/config"
	"werematrix/bot/constants"
	"werematrix/bot/log"
	"werematrix/bot/routing"
)

// StartBot starts the bot
func StartBot() {
	log.Info("Starting bot...")
	client, err := gomatrix.NewClient(Config.Bot.Homeserver, "", "")
	if err != nil {
		log.Fatal(constants.StartupFailure, "failure connecting to homeserver: %+v", err)
	}

	resp, err := client.Login(&gomatrix.ReqLogin{
		Type:     "m.login.password",
		User:     Config.Bot.Username,
		Password: Config.Bot.Password,
	})
	if err != nil {
		log.Fatal(constants.StartupFailure, "failure logging in: %+v", err)
	}

	client.SetCredentials(resp.UserID, resp.AccessToken)
	client.UserID = resp.UserID

	r := routing.NewRouter(client)
	commands.RegisterCommands(&r)

	syncer := client.Syncer.(*gomatrix.DefaultSyncer)
	syncer.OnEventType("m.room.message", r.HandleMessage)
	syncer.OnEventType("m.room.member", func(ev *gomatrix.Event) {
		if val, ok := ev.Content["membership"]; ok {
			if val.(string) == "invite" {
				client.JoinRoom(ev.RoomID, "", nil)
			}
		}
	})

	done := make(chan struct{})

	go func() {
		log.Info("Starting event syncing...")
		for {
			err := client.Sync()
			if err != nil {
				log.Error("Error syncing: %+v", err)
				break
			}
			time.Sleep(time.Millisecond * 500)
		}
		done <- struct{}{}
	}()

	<-done
}
