package commands

import (
	"werematrix/bot/db"
	"werematrix/bot/routing"
	"werematrix/bot/types"
)

func requireGameRunning(h routing.Handler) routing.Handler {
	return func(client *types.Context, event *types.Event) error {
		if _, ok := states.State(event.RoomID); !ok {
			client.SendMessage("No game appears to be running.")
			return nil
		}
		return h(client, event)
	}
}

func requireGameNotStarted(h routing.Handler) routing.Handler {
	return func(client *types.Context, event *types.Event) error {
		if states.StateX(event.RoomID).Running() {
			client.SendMessage("You can only use this command when the game hasn't started yet.")
			return nil
		}
		return h(client, event)
	}
}

func requireCreator(h routing.Handler) routing.Handler {
	return func(client *types.Context, event *types.Event) error {
		if states.StateX(event.RoomID).Creator() != event.Sender {
			client.SendMessage("You can only use this command if you created the game.")
			return nil
		}
		return h(client, event)
	}
}

func requireCommandsRoom(h routing.Handler) routing.Handler {
	return func(client *types.Context, event *types.Event) error {
		if db.User(event.Sender).CommandsRoom == "" {
			client.SendMessage(
				"You haven't configured a commands room! Start a DM with me and type '-werewolf room' to set a private commands room.",
			)
			return nil
		}
		return h(client, event)
	}
}
