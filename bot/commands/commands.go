package commands

import (
	"werematrix/bot/log"
	"werematrix/bot/routing"
	"werematrix/bot/state"
)

var cmds []*routing.Command
var states = (&state.States{}).Init()

// RegisterCommands registers commands with the given router
func RegisterCommands(r *routing.Router) {
	log.Info("Registering commands...")
	defer log.Info("%d commands registered", len(cmds))

	for _, cmd := range cmds {
		r.RegisterCommand(cmd)
	}
}
