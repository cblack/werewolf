package commands

import (
	"context"
	"werematrix/bot/db"
	"werematrix/bot/routing"
	"werematrix/bot/types"
)

func init() {
	cmds = append(cmds, &routing.Command{
		Matches: []string{"-werewolf room"},
		Handler: preferRoom,
	})
}

func preferRoom(client *types.Context, event *types.Event) (err error) {
	_, err = db.User(event.Sender).Update().SetCommandsRoom(event.RoomID).Save(context.Background())
	if err != nil {
		return err
	}

	client.SendMessage(
		"This room is now your preferred room! Private status updates and commands will be located here from now on.",
	)

	return
}
