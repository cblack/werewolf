package commands

import (
	"fmt"
	"strings"
	"werematrix/bot/routing"
	"werematrix/bot/types"
)

func startGame(client *types.Context, event *types.Event) (err error) {
	_, ok := states.State(event.RoomID)
	if ok {
		client.SendMessage("Looks like there's a game already ongoing.")
		return
	}

	states.CreateState(event.RoomID, event.Sender)
	client.SendMessage(
		"A new game has started! Type '-werewolf join' to join! (To the creator: When you're ready to start, type '-werewolf begin' to start.",
	)

	return
}

func init() {
	cmds = append(cmds, &routing.Command{
		Matches:     []string{"-werewolf start"},
		Handler:     startGame,
		Middlewares: []routing.Middleware{requireCommandsRoom},
	})
}

func joinGame(client *types.Context, event *types.Event) (err error) {
	if states.StateX(event.RoomID).Join(event.Sender) {
		client.SendMessage(
			"You joined the game!",
		)
	} else {
		client.SendMessage(
			"You are already in the game.",
		)
	}

	return
}

func init() {
	cmds = append(cmds, &routing.Command{
		Matches:     []string{"-werewolf begin"},
		Handler:     beginGame,
		Middlewares: []routing.Middleware{requireGameRunning, requireGameNotStarted, requireCreator},
	})
}

func beginGame(client *types.Context, event *types.Event) (err error) {
	states.StateX(event.RoomID).Begin()
	client.SendMessage(
		"The game has begun!",
	)

	states.StateX(event.RoomID).PrepareUserData()

	var s []string
	for user, role := range states.StateX(event.RoomID).GetRoles() {
		s = append(s, fmt.Sprintf("%s - %s", user, role))
	}

	client.SendMessage(
		strings.Join(s, "\n"),
	)

	return
}

func init() {
	cmds = append(cmds, &routing.Command{
		Matches:     []string{"-werewolf join"},
		Handler:     joinGame,
		Middlewares: []routing.Middleware{requireCommandsRoom, requireGameRunning, requireGameNotStarted},
	})
}

func fleeGame(client *types.Context, event *types.Event) (err error) {
	if states.StateX(event.RoomID).Creator() == event.Sender {
		client.SendMessage(
			"You created the game, you can't flee!",
		)
		return
	}
	if states.StateX(event.RoomID).Unjoin(event.Sender) {
		client.SendMessage(
			"You fled the game!",
		)
	} else {
		client.SendMessage(
			"You aren't in the game, mate.",
		)
	}

	return
}

func init() {
	cmds = append(cmds, &routing.Command{
		Matches:     []string{"-werewolf flee"},
		Handler:     fleeGame,
		Middlewares: []routing.Middleware{requireGameRunning},
	})
}
