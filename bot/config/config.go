package config

import (
	"werematrix/bot/constants"
	"werematrix/bot/log"

	"github.com/BurntSushi/toml"
)

// BConfig contains the bot's configuration data
type BConfig struct {
	// Bot contains bot-related config
	Bot struct {
		Homeserver string `toml:"homeserver"`
		Username   string `toml:"username"`
		Password   string `toml:"password"`
	} `toml:"bot"`
}

// Config is the instance of BConfig loaded from config.toml at startup
var Config BConfig

func init() {
	log.Info("Reading config...")
	_, err := toml.DecodeFile("config.toml", &Config)
	if err != nil {
		log.Fatal(constants.ConfigFailure, "Failed to read config: %+v", err)
	}
}
