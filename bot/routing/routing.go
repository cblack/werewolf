package routing

import (
	"fmt"
	"werematrix/bot/types"

	iradix "github.com/hashicorp/go-immutable-radix"
	"github.com/matrix-org/gomatrix"
)

// Router is responsible for routing commands
type Router struct {
	c            *gomatrix.Client
	client       *types.Client
	commands     *iradix.Tree
	commandsList []*Command
}

// NewRouter creates a new Router
func NewRouter(client *gomatrix.Client) Router {
	return Router{
		c:        client,
		commands: iradix.New(),
		client:   &types.Client{Client: client},
	}
}

// HandleMessage handles an incoming matrix event
func (r *Router) HandleMessage(event *gomatrix.Event) {
	if _, ok := event.Body(); !ok {
		return
	}

	ev := types.Event{Event: event}

	_, val, ok := r.commands.Root().LongestPrefix([]byte(ev.Body()))
	if !ok {
		return
	}

	cmd := val.(*Command)

	err := cmd.ChainedHandler()(&types.Context{
		Client: r.client,
		RoomID: event.RoomID,
	}, &ev)
	if err != nil {
		r.client.SendMessage(event.RoomID, fmt.Sprintf("There was an error handling your request: %+v", err))
	}
}

// RegisterCommand registers a command with the router
func (r *Router) RegisterCommand(c *Command) {
	for _, match := range c.Matches {
		r.commands, _, _ = r.commands.Insert([]byte(match), c)
	}
	r.commandsList = append(r.commandsList, c)
}
