package routing

import "werematrix/bot/types"

// Handler handles a response
type Handler func(client *types.Context, event *types.Event) error

// Middleware wraps a handler
type Middleware func(Handler) Handler

// Command holds the definition of a command
type Command struct {
	Matches []string

	Handler     Handler
	Middlewares []Middleware

	computed Handler
}

// ChainedHandler processes chained handlers
func (c Command) ChainedHandler() Handler {
	if c.computed != nil {
		return c.computed
	}

	handler := c.Handler
	for i := len(c.Middlewares) - 1; i >= 0; i-- {
		handler = c.Middlewares[i](handler)
	}
	c.computed = handler

	if handler == nil {
		panic("handler is nil")
	}

	return handler
}
