package types

import "github.com/matrix-org/gomatrix"

// Event wraps a *gomatrix.Event and provides convenience methods
type Event struct {
	*gomatrix.Event
}

// Body returns the body of an event
func (e Event) Body() string {
	ev, ok := e.Event.Body()
	if !ok {
		panic("unexpected not ok")
	}
	return ev
}

// Client wraps a *gomatrix.Client and provides convenience methods
type Client struct {
	*gomatrix.Client
}

type matrixMessage struct {
	Format string `json:"format,omitempty"`
	HTML   string `json:"formatted_body,omitempty"`
	Body   string `json:"body,omitempty"`
	Type   string `json:"msgtype,omitempty"`
}

// SendMessage sends a message
func (c Client) SendMessage(roomID string, content string) {
	c.Client.SendMessageEvent(roomID, "m.room.message", matrixMessage{
		Body: content,
		Type: "m.text",
	})
}

// Context provides a single-room-oriented view on an event
type Context struct {
	*Client
	RoomID string
}

// SendMessage sends a message to the room the user was in
func (c Context) SendMessage(content string) {
	c.Client.SendMessage(c.RoomID, content)
}
