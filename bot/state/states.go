package state

import "sync"

// States holds multiple states, with a mutex
type States struct {
	mtx    sync.RWMutex
	states map[string]*State
}

// Init inits the States structure to reasonable defaults
func (s *States) Init() *States {
	s.states = map[string]*State{}

	return s
}

// State returns a given state by ID if it exists
func (s *States) State(id string) (*State, bool) {
	s.mtx.RLock()
	defer s.mtx.RUnlock()

	state, ok := s.states[id]
	return state, ok
}

// StateX is like State, but panics if state does not exist
func (s *States) StateX(id string) *State {
	state, ok := s.State(id)
	if !ok {
		panic("state not found")
	}
	return state
}

// CreateState creates a state with the given ID
func (s *States) CreateState(roomID, userID string) *State {
	_, ok := s.states[roomID]
	if ok {
		panic("state already exists")
	}

	s.states[roomID] = &State{
		users:    map[string]struct{}{},
		userData: map[string]userData{},
		creator:  userID,
	}
	s.states[roomID].Join(userID)

	return s.states[roomID]
}
