package state

import (
	"strings"
	"werematrix/bot/types"
)

type UserDetails struct {
	Name         string
	Description  string
	WinCondition string
}

type Hook func(*State, *types.Context)
type EventHook func(string, *State, *types.Context)

type Role struct {
	ID                   string
	Evil                 bool
	UserFacing           UserDetails
	EvaluateWinCondition func(*State) bool

	OnActionPhaseEnter Hook
	ActionPhaseEvents  EventHook
	OnActionPhaseEnd   Hook
}

// GetRole returns a role
func GetRole(roleID string) Role {
	return roles[roleID]
}

var roles map[string]Role

func init() {
	roles = map[string]Role{
		"villager": {
			ID: "villager",
			UserFacing: UserDetails{
				Name:         "Villager",
				Description:  "A boring old villager.",
				WinCondition: "Don't die, and get rid of all the evil people.",
			},
			EvaluateWinCondition: noEvilWinCondition,
		},
		"werewolf": {
			ID: "werewolf",
			UserFacing: UserDetails{
				Name:         "Villager",
				Description:  "A ferocious werewolf.",
				WinCondition: "Kill everyone but the other werewolves.",
			},
			EvaluateWinCondition: onlyRoleID("werewolf"),
			OnActionPhaseEnter: func(s *State, c *types.Context) {
				c.SendMessage("Pick someone to kill by sending their Matrix username (mine is @werewolfbot:kde.org).")
			},
			ActionPhaseEvents: func(contents string, s *State, c *types.Context) {
				_, ok := s.users[strings.TrimSpace(contents)]
				if !ok {
					c.SendMessage("That user isn't in this game!")
				}
				s.stagedUserDeath = strings.TrimSpace(contents)
				c.SendMessage("Tonight, you're going to kill " + contents + "!")
			},
		},
	}
}

func noEvilWinCondition(s *State) bool {
	for _, user := range s.userData {
		if user.living && roles[user.roleID].Evil {
			return false
		}
	}
	return true
}

func onlyRoleID(role string) func(s *State) bool {
	return func(s *State) bool {
		for _, user := range s.userData {
			if user.living && roles[user.roleID].ID != role {
				return false
			}
		}

		return true
	}
}
