package state

type userData struct {
	roleID string
	living bool
}

// State represents the state for a given room
type State struct {
	users    map[string]struct{}
	userData map[string]userData
	creator  string

	running bool

	stagedUserDeath string
}

// GetRoles returns a list of user roles
func (s *State) GetRoles() (d map[string]string) {
	d = make(map[string]string)

	for k, v := range s.userData {
		d[k] = v.roleID
	}

	return
}

// Begin the game. Panics if it's already started.
func (s *State) Begin() {
	if s.running {
		panic("began game more than once")
	}
	s.running = true
}

// Running returns whether or not the game is running
func (s *State) Running() bool {
	return s.running
}

// Creator returns the ID of the user that created this room
func (s *State) Creator() string {
	return s.creator
}

// Join joins a user to the room state. Ok if the user was just joined, or
// false if already joined.
func (s *State) Join(userID string) (ok bool) {
	if _, ok := s.users[userID]; ok {
		return false
	}

	s.users[userID] = struct{}{}
	return true
}

// Unjoin unjoins a user to the room state. Ok if the user was unjoined, or
// false if the user was not in the room state.
func (s *State) Unjoin(userID string) (ok bool) {
	if _, ok := s.users[userID]; !ok {
		return false
	}

	delete(s.users, userID)
	return true
}

// Joined returns whether or not a user has joined
func (s *State) Joined(userID string) (ok bool) {
	_, ok = s.users[userID]

	return ok
}
