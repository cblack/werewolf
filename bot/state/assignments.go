package state

// PrepareUserData prepares the user data of a state, assigning roles
func (s *State) PrepareUserData() {
	for user := range s.users {
		var role Role

		for _, r := range roles {
			role = r
			break
		}

		s.userData[user] = userData{
			roleID: role.ID,
			living: true,
		}
	}
}
