package constants

const (
	// ConfigFailure is the exit code when loading configuration fails
	ConfigFailure int = iota + 1
	// StartupFailure is the exit code when starting up fails
	StartupFailure
)
