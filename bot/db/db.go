package db

import (
	"context"
	"werematrix/bot/constants"
	"werematrix/bot/log"
	"werematrix/ent"

	_ "github.com/mattn/go-sqlite3"
)

// Client is the loaded client instnace
var Client *ent.Client

func init() {
	log.Info("Loading database...")
	client, err := ent.Open("sqlite3", "file:data.db?cache=shared&_fk=1")
	if err != nil {
		log.Fatal(constants.StartupFailure, "Failed to open database: %s", err)
	}
	log.Info("Migrating database...")
	client.Schema.Create(context.Background())

	Client = client
	log.Info("Database ready!")
}

// User returns a handle to a user database object
func User(userID string) *ent.User {
	user, err := Client.User.Get(context.Background(), userID)
	if err != nil {
		if ent.IsNotFound(err) {
			return Client.User.Create().SetID(userID).SaveX(context.Background())
		}
		panic(err)
	}

	return user
}
